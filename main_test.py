import unittest
import sys
import io
import main

class Main_test(unittest.TestCase):
	def test_1(self):
		input_str: str = '''10 4 3\n1 2 3 4 1 2 3 1 2 1'''
		output_str: str = '''4 1 10 9 4 \n3 5 2 3 \n3 8 6 7 \n'''
		in_b = io.StringIO(input_str)
		out_b = io.StringIO()
		main.main(stdin=in_b, stdout=out_b)
		real_output = out_b.getvalue()
		self.assertEqual(real_output, output_str)


if __name__ == '__main__':
	test = Main_test()
	test.test_1()