import sys
from typing import List
from io import TextIOWrapper

def main(argv: List[str] = sys.argv, stdin: TextIOWrapper = sys.stdin, stdout: TextIOWrapper = sys.stdout, stderr: TextIOWrapper = sys.stderr):
    n, m, k = map(int, stdin.readline().split())
    clases = list(map(int, stdin.readline().split()))
    clases_pair = [(clases[i], i) for i in range(0, len(clases))]
    clases_pair.sort()
    k_lists = [[] for i in range(0, k)]
    j = 0
    for i in range(len(clases)):
        j = j % k
        k_lists[j].append((clases_pair[i]))
        j += 1
    for i in range(k):
        print(len(k_lists[i]), end=" ", file=stdout)
        for j in range(0, len(k_lists[i])):
            print(int(k_lists[i][j][1]) + 1, end=" ", file=stdout)
        print(file=stdout)

if __name__ == '__main__':
    main()